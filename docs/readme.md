# Documentation

## Business domain

For analyze business domain I was used event storming, result and descripton you can read in [dosc/event-storming.md](./event-storming.md)

## Architecture Decision Record(ADR)

For description decision and save arguments I will create ADR documents. More information about this documents and template you can read in [docs/ADR/adr-template.md](./adr-template.md)