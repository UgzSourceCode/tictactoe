# Architecture Decision Record template

This is the template based on [Documenting architecture decisions - Michael Nygard](http://thinkrelevance.com/blog/2011/11/15/documenting-architecture-decisions).

In each ADR file, write these sections:

# Title

## Status

What is the status, such as proposed, accepted, rejected, deprecated, superseded, etc.?

## Context

What is the issue that we're seeing that is motivating this decision or change?

## Options

What options do you have? Do research and create list with your options.

### Example options

Positive Consequences:
- first consequence
- second consequence

Negative Consequences:
- first consequence
- second consequence

## Decision

What is the change that we're proposing and/or doing?

## Consequences

What becomes easier or more difficult to do because of this chan