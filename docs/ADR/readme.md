# Architecture Decision Record(ADR)

## Template

I will use template based on [decision-record-template-by-michael-nygard](https://github.com/joelparkerhenderson/architecture-decision-record/blob/main/templates/decision-record-template-by-michael-nygard/index.md).

In document [docs/ADR/adr-template.md](./adr-template.md) you can see template which I will use in this project.

## List of ADR documents

### ADR example item

Link: [docs/ADR/readme.md](./readme.md)

Decision: Example decision